import os
import random
import xml.etree.ElementTree as ET

def modify_glif_files(directory):
    # Iterate over each file in the directory
    for filename in os.listdir(directory):
        if filename.endswith(".glif"):
            filepath = os.path.join(directory, filename)
            tree = ET.parse(filepath)
            root = tree.getroot()

            # Iterate over each <component> element
            for component in root.findall(".//component"):
                # Modify xOffset and yOffset attributes
                xOffset = component.attrib.get("xOffset")
                yOffset = component.attrib.get("yOffset")

                if xOffset:
                    xOffset = float(xOffset) + random.uniform(-25, 25)
                    component.set("xOffset", str(xOffset))
                if yOffset:
                    yOffset = float(yOffset) + random.uniform(-25, 25)
                    component.set("yOffset", str(yOffset))

            # Write the modified XML back to the file
            tree.write(filepath)

if __name__ == "__main__":
    glif_files_directory = "fontfile.ufo/glyphs"
    modify_glif_files(glif_files_directory)
